#!/usr/bin/env python3

#
# Copyright (C) 2015 Jens Steinhauser <jens.steinhauser@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
#

from math import atan2, cos, pi, sin, sqrt
import argparse
import collections
import io
import lxml.etree as ET
import sys
import textwrap

# Throughout this script, coordinates are represented as (lat, lon) tupes.
# For the distance and bearing calculation see:
# http://www.movable-type.co.uk/scripts/latlong.html

def distance(c1, c2):
    '''Calculates the distance between two coordinates.'''
    c1_lat, c1_lon = c1
    c2_lat, c2_lon = c2

    r = 6371000
    phi1 = c1_lat * pi / 180
    phi2 = c2_lat * pi / 180
    dphi = (c2_lat - c1_lat) * pi / 180
    dlam = (c2_lon - c1_lon) * pi / 180

    a = pow(sin(dphi/2), 2) + \
        cos(phi1) * cos(phi2) * \
        pow(sin(dlam/2), 2)

    c = 2 * atan2(sqrt(a), sqrt(1 - a))
    return r * c

def bearing(c1, c2):
    '''Calculates the bearing between two coordinates.'''
    c1_lat, c1_lon = c1
    c2_lat, c2_lon = c2

    phi1 = c1_lat * pi / 180
    phi2 = c2_lat * pi / 180
    dphi = (c2_lat - c1_lat) * pi / 180
    dlam = (c2_lon - c1_lon) * pi / 180

    y = sin(dlam) * cos(phi2)
    x = cos(phi1) * sin(phi2) - sin(phi1) * cos(phi2) * cos(dlam)

    return atan2(y, x)

class VoGISKML:
    '''Class to handle a KML file that was converted from the VoGIS shapefile.

    This class doesn't try to be exhaustive and only covers stuff that is found
    in the KML files that are convertet from the VoGIS shapefiles using ogr2ogr.
    '''

    ns = {'ns': 'http://www.opengis.net/kml/2.2'}

    def __init__(self, filename):
        self.doc = ET.parse(filename)

        schema_doc = ET.parse('schemas/ogckml22.xsd')
        schema = ET.XMLSchema(schema_doc)
        schema.assertValid(self.doc)

        root = self.doc.getroot()

        #
        # parse the data schema
        #

        self.schemas = {}
        for sch in root.xpath('ns:Document/ns:Schema', namespaces=VoGISKML.ns):
            d = {}
            for field in sch.xpath('ns:SimpleField', namespaces=VoGISKML.ns):
                st = field.attrib['type']
                if st == 'string':
                    t = str
                elif st in ('int', 'uint', 'short', 'ushort'):
                    t = int
                elif st in ('float', 'double'):
                    t = float
                elif st == 'bool':
                    t = bool
                else:
                    raise RuntimeError('unknown type "{}"'.format(st))

                d[field.attrib['name']] = t

            self.schemas['#' + sch.attrib['id']] = d

        #
        # parse the lines
        #

        def parse_coordinate(s):
            lon, lat = s.split(',')
            return (float(lat), float(lon))

        self.lines = []
        for l in root.xpath('ns:Document/ns:Folder/ns:Placemark[//ns:LineString]',
                            namespaces=VoGISKML.ns):

            metadata = self._schemaData(l)

            coords = l.xpath('.//ns:LineString/ns:coordinates/text()',
                             namespaces=VoGISKML.ns)

            if len(coords) == 1:
                coords = coords[0]
            else:
                objid = metadata.get('OBJECTID', '<no id found>')
                if len(coords) == 0:
                    print('object #{}: no coordinates in LineString'.format(objid))
                    continue
                else:
                    print('object #{}: picking longest LineString'.format(objid))
                    coords = max(coords, key=len)

            coords = coords.split()
            coords = [parse_coordinate(c) for c in coords]

            self.lines.append((metadata, coords))

        #
        # parse the points
        #

        self.points = []
        for p in root.xpath('ns:Document/ns:Folder/ns:Placemark[ns:Point]',
                            namespaces=VoGISKML.ns):

            metadata = self._schemaData(p)

            coords = p.xpath('ns:Point/ns:coordinates/text()',
                             namespaces=VoGISKML.ns)

            if len(coords) != 1:
                objid = metadata.get('OBJECTID', '<no id found>')
                print('object #{}: wrong number of coordinates'.format(objid))
                continue

            coords = parse_coordinate(coords[0])

            self.points.append((metadata, coords))

    def _schemaData(self, placemark):
        result = {}
        for data in placemark.xpath('ns:ExtendedData/ns:SchemaData',
                                    namespaces=VoGISKML.ns):
            url = data.attrib['schemaUrl']
            if not url in self.schemas:
                raise RuntimeError('invalid schema url "{}"'.format(url))
            schema = self.schemas[url]

            for simple in data.xpath('ns:SimpleData',
                                     namespaces=VoGISKML.ns):
                name = simple.attrib['name']
                if not name in schema:
                    raise RuntimeError('invalid data name "{}"'.format(name))

                result[name] = schema[name](simple.text)

        return result

def extract_trails(kml):
    '''Returns the trails contained in the KML file.'''

    # hiking, mountain, and alpine trails
    trails = ([], [], [])
    for metadata, coords in kml.lines:
        kind = metadata.get('WEG_ART')
        objid = metadata.get('OBJECTID', '<no id found>')
        if kind == None:
            print('object #{}: no WEG_ART'.format(objid))
            continue
        elif kind < 1 or 3 < kind:
            print('object #{}: invalid WEG_ART "{}"'.format(objid, kind))
            continue

        trails[kind - 1].append((str(objid), coords))

    return trails

def extract_guideposts(kml):
    '''Returns the guideposts contained in the KML file.'''
    result = []
    for metadata, coords in kml.points:
        nr = metadata.get('TAFEL_NR', '')
        ele = metadata.get('TAFEL_HOEH', '')

        if nr == '' or ele == '':
            objid = metadata.get('OBJECTID', '<no id found>')
            print('guidepost object #{}: no TAFEL_NR and/or TAFEL_HOEH'.format(objid))

        result.append((nr, ele, coords))

    return result

def reduce_trail(trail):
    '''Reduces excessive points from a trail.'''
    name, coords = trail

    # a point is taken if it's more than this away from the last point
    limitd = 150 # in meters

    # a point is taken if the outgoint bearing is bigger that
    # the one of the last taken point
    limitb = 30 * pi / 180

    # the first one is always included
    result = [coords[0]]

    lastb = bearing(coords[0], coords[1])

    for i in range(1, len(coords) - 1):
        b = bearing(coords[i], coords[i + 1])
        d = distance(result[-1], coords[i])

        if abs(b - lastb) > limitb or d > limitd:
            # take the current point
            result.append(coords[i])
            lastb = b

    # the last one is always included
    result.append(coords[-1])

    return (name, result)

def cropfunction(bb):
    llon = min(bb[0][1], bb[1][1])
    rlon = max(bb[0][1], bb[1][1])
    blat = min(bb[0][0], bb[1][0])
    tlat = max(bb[0][0], bb[1][0])

    return lambda c: not (c[1] < llon or c[1] > rlon or c[0] < blat or c[0] > tlat)

def crop_trails(bb, trails):
    '''Removes trails and part of trails that are outside of the bounding box.'''
    cf = cropfunction(bb)
    cropped = [(tr[0], [c for c in tr[1] if cf(c)]) for tr in trails]

    # remove trails that became too short
    return [t for t in cropped if len(t[1]) > 1]

def crop_guideposts(bb, guideposts):
    '''Removes guideposts that are outside of the bounding box.'''
    cf = cropfunction(bb)
    return [gp for gp in guideposts if cf(gp[2])]

def kml_output_skeleton():
    template = '''
    <kml xmlns="http://www.opengis.net/kml/2.2">
        <Document id="root_doc">
            <Style id="sh">
                <LineStyle>
                    <color>ff0000ff</color>
                </LineStyle>
            </Style>
            <Style id="sm">
                <LineStyle>
                    <color>ff00baff</color>
                </LineStyle>
            </Style>
            <Style id="sa">
                <LineStyle>
                    <color>ff00fcff</color>
                </LineStyle>
            </Style>
            <Style id="sg">
                <IconStyle >
                    <Icon>
                        <href>http://maps.google.com/mapfiles/kml/shapes/placemark_square.png</href>
                    </Icon>
                </IconStyle>
            </Style>
        </Document>
    </kml>
    '''

    parser = ET.XMLParser(remove_blank_text=True)
    et = ET.parse(io.StringIO(template), parser)

    root = et.getroot()
    document = root.find('{http://www.opengis.net/kml/2.2}Document')

    fh = ET.SubElement(document, 'Folder')
    name = ET.SubElement(fh, 'name')
    name.text = 'hiking trails'

    fm = ET.SubElement(document, 'Folder')
    name = ET.SubElement(fm, 'name')
    name.text = 'mountain trails'

    fa = ET.SubElement(document, 'Folder')
    name = ET.SubElement(fa, 'name')
    name.text = 'alpine trails'

    fg = ET.SubElement(document, 'Folder')
    name = ET.SubElement(fg, 'name')
    name.text = 'guideposts'
    visibility = ET.SubElement(fg, 'visibility')
    visibility.text = '0'

    return (et, [fh, fm, fa, fg])

def write_kml_output(filename, trails, guideposts, precision=16):
    et, folders = kml_output_skeleton()

    c2str = lambda p: '{:.{prec}f},{:.{prec}f}'.format(p[1], p[0], prec=precision)

    # write trails
    for i in range(3):
        folder = folders[i]
        style = ['#sh', '#sm', '#sa'][i]

        for trailname, coords in trails[i]:
            placemark = ET.SubElement(folder, 'Placemark')

            name = ET.SubElement(placemark, 'name')
            name.text = trailname

            styleurl = ET.SubElement(placemark, 'styleUrl')
            styleurl.text = style

            linestring = ET.SubElement(placemark, 'LineString')
            coordinates = ET.SubElement(linestring, 'coordinates')
            coordinates.text = ' '.join([c2str(c) for c in coords])

    # write guideposts
    for nr, ele, coord in guideposts:
        c = c2str(coord)
        if ele != None:
            c += ',{}'.format(ele)

        placemark = ET.SubElement(folders[3], 'Placemark')

        name = ET.SubElement(placemark, 'name')
        name.text = nr

        visibility = ET.SubElement(placemark, 'visibility')
        visibility.text = '0'

        styleurl = ET.SubElement(placemark, 'styleUrl')
        styleurl.text = '#sg'

        point = ET.SubElement(placemark, 'Point')
        coordinates = ET.SubElement(point, 'coordinates')
        coordinates.text = c

    print('writing output file "{}"'.format(filename))
    et.write(filename, encoding='utf-8', xml_declaration=True, pretty_print=True)

def write_osm_output(filename, trails, guideposts):
    et = ET.ElementTree()
    et.parse(io.StringIO("<osm version='0.6' />"))
    root = et.getroot()

    class OSMNode:
        def __init__(self, nid):
            self.nid = nid
            self.coordinate = None
            self.tags = []

    def id_generator():
        lastid = 0
        while True:
            lastid -= 1
            yield lastid

    def node_generator(id_gen):
        while True:
            yield OSMNode(next(id_gen))

    id_gen = id_generator()
    node_gen = node_generator(id_gen)

    # a dictionary to hold all the OSM nodes
    nodes = collections.defaultdict(lambda: next(node_gen))
    def lookup_node(coordinate):
        '''Creates a new node, or returns an existing one if one exists at
        approximately the same position.'''
        k = (int(coordinate[0] * 1000000),
             int(coordinate[1] * 1000000))
        nonlocal nodes
        node = nodes[k]
        node.coordinate = coordinate # when the node is newly created
        return node

    # generate nodes for the guideposts
    for nr, ele, coordinate in guideposts:
        node = lookup_node(coordinate)
        node.tags += [
            ('tourism', 'information'),
            ('information', 'guidepost'),
            ('ref', nr),
            ('ele', str(ele))
        ]

    ways = []

    # make sure the nodes on all ways are created
    for i in range(3):
        sac_scale = ['hiking', 'mountain_hiking', 'alpine_hiking'][i]

        for trailname, trail in trails[i]:
            # ids of all nodes on the current way
            nodeids = [lookup_node(c).nid for c in trail]
            ways.append((trailname, sac_scale, nodeids))

    def add_tag(node, key, value):
        '''Adds a tag to a OSM XML element.'''
        tag = ET.SubElement(node, 'tag')
        tag.attrib['k'] = key
        tag.attrib['v'] = value

    # generate XML for all the nodes
    for node in nodes.values():
        nele = ET.SubElement(root, 'node')
        nele.attrib['id'] = str(node.nid)
        nele.attrib['lat'] = str(node.coordinate[0])
        nele.attrib['lon'] = str(node.coordinate[1])
        # add attributes that osmosis needs
        nele.attrib['version'] = '1'
        nele.attrib['timestamp'] = '2008-00-00T00:00:00Z'

        for tag in node.tags:
            add_tag(nele, tag[0], tag[1])

    # the ways have to come after the nodes
    for name, sac_scale, way in ways:
        wid = next(id_gen)
        w = ET.SubElement(root, 'way')
        w.attrib['id'] = str(wid)
        # add attributes that osmosis needs
        w.attrib['version'] = '1'
        w.attrib['timestamp'] = '2008-00-00T00:00:00Z'

        for node in way:
            n = ET.SubElement(w, 'nd')
            n.attrib['ref'] = str(node)

        add_tag(w, 'highway', 'path')
        add_tag(w, 'ref', name)
        add_tag(w, 'sac_scale', sac_scale)

    print('writing output file "{}"'.format(filename))
    et.write(filename, encoding='utf-8', xml_declaration=True, pretty_print=True)

def parse_cli():
    parser = argparse.ArgumentParser(
        description=
            'Simple tool to convert the hiking trails found on the VoGIS sftp server.',
        epilog=textwrap.dedent('''\
            The program creates four output files:

                <OUTPREFIX>.kml
                    Includes all points and full precision for the coordinates.
                <OUTPREFIX>_decimated.kml
                    Some points from the trails are removed, five decimal places
                    for the coordinates are used, which doesn't introduce a big error.
                <OUTPREFIX>.osm & <OUTPREFIX>_decimated.osm
                    The full output and the decimated output in OSM format.

            The BBOX argument should have the following format:

                lot1,lat1,lot2,lat2

            You can use http://boundingbox.klokantech.com to define the box graphically,
            then use the CSV output at the bottom of the page.
        '''),
        formatter_class=argparse.RawDescriptionHelpFormatter
    )
    parser.add_argument('waysfile',
        metavar='INFILE_WAYS',
        type=str,
        help='The input KML file containing the hiking trails.')
    parser.add_argument('guidepostsfile',
        metavar='INFILE_GUIDEPOSTS',
        type=str,
        help='The input KML file containing the guideposts.')
    parser.add_argument('outprefix',
        metavar='OUTPREFIX',
        type=str,
        help='Prefix that starts the name of the output files.')
    parser.add_argument('-b', '--bbox',
        type=str,
        default=None,
        help='Crop output to bounding box.')

    try:
        args = parser.parse_args()

        if args.bbox != None:
            l = args.bbox.split(',')
            if len(l) != 4:
                raise RuntimeError('wrong format for --bbox argument')

            lat1 = float(l[1])
            lon1 = float(l[0])
            lat2 = float(l[3])
            lon2 = float(l[2])

            args.bbox = ((lat1, lon1), (lat2, lon2))
    except Exception as e:
        sys.exit('error parsing command line: {}'.format(e))

    return args

if __name__ == '__main__':
    args = parse_cli()

    try:
        ways_kml = VoGISKML(args.waysfile)
        trails = extract_trails(ways_kml)

        guideposts_kml = VoGISKML(args.guidepostsfile)
        guideposts = extract_guideposts(guideposts_kml)
    except Exception as e:
        sys.exit('error reading input file: {}'.format(e))

    print('found {} hiking trials'.format(len(trails[0])))
    print('found {} mountain trials'.format(len(trails[1])))
    print('found {} alpine trials'.format(len(trails[2])))
    print('found {} guideposts'.format(len(guideposts)))

    if args.bbox:
        print('cropping to bounding box')
        trails = (
            crop_trails(args.bbox, trails[0]),
            crop_trails(args.bbox, trails[1]),
            crop_trails(args.bbox, trails[2])
        )
        guideposts = crop_guideposts(args.bbox, guideposts)

        print('after cropping: {} hiking trials'.format(len(trails[0])))
        print('after cropping: {} mountain trials'.format(len(trails[1])))
        print('after cropping: {} alpine trials'.format(len(trails[2])))
        print('after cropping: {} guideposts'.format(len(guideposts)))

    write_kml_output('{}.kml'.format(args.outprefix), trails, guideposts)
    write_osm_output('{}.osm'.format(args.outprefix), trails, guideposts)

    trails = (
        [reduce_trail(trail) for trail in trails[0]],
        [reduce_trail(trail) for trail in trails[1]],
        [reduce_trail(trail) for trail in trails[2]]
    )

    write_kml_output('{}_decimated.kml'.format(args.outprefix), trails, guideposts, precision=5)
    write_osm_output('{}_decimated.osm'.format(args.outprefix), trails, guideposts)
