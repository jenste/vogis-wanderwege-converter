#!/usr/bin/env python3

#
# Copyright (C) 2015 Jens Steinhauser <jens.steinhauser@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
#

import datetime
import os
import paramiko
import re
import stat
import sys

host = 'vogis.cnv.at'
port = 22
username = 'vogis'
password = 'vogisdaten'

# remote directory to check/download
directory = 'Verkehr/Weg/Vlbg/Wanderweg'

# prefix of the output directories
prefix = 'vogis-wandern_'

def getdir(sftp, rdir, ldir):
    '''Download all files in 'rdir' to 'ldir'.'''
    def cb(filename, done, total):
        print('\r{:3}% {}'.format(int(100 * done / total), filename), end='')
        sys.stdout.flush()

    if os.path.exists(ldir):
        raise RuntimeError('local directory already exists')

    os.mkdir(ldir)

    sys.stdout.flush()
    for attr in sftp.listdir_attr(path=directory):
        if not stat.S_ISREG(attr.st_mode):
            print('"{}": not a regular file, skipping'.format(attr.filename))
            sys.stdout.flush()

        rem = os.path.join(rdir, attr.filename)
        loc = os.path.join(ldir, attr.filename)
        sftp.get(rem, loc, lambda d, t: cb(attr.filename, d, t))
        print('')
        sys.stdout.flush()

def newest_local():
    '''Returns the latest downloaded version (or None).'''
    dates = []
    for f in os.listdir():
        pattern = prefix + '(\d{4})-(\d{2})-(\d{2})_(\d{2})_(\d{2})_(\d{2})$'
        m = re.match(pattern, f)
        if m:
            d = {
                'year': int(m.group(1)),
                'month': int(m.group(2)),
                'day': int(m.group(3)),
                'hour': int(m.group(4)),
                'minute': int(m.group(5)),
                'second': int(m.group(6))
            }
            dates.append(datetime.datetime(**d))

    if dates:
        return max(dates)
    return None

def newest_remote(sftp):
    '''Returns the latest available version.'''
    # the modification time of the newest file
    newest = max([a.st_mtime for a in sftp.listdir_iter(path=directory)])
    return datetime.datetime.fromtimestamp(newest)

if __name__ == '__main__':
    try:
        transport = paramiko.Transport((host, port))
        transport.connect(username=username, password=password)
        sftp = paramiko.SFTPClient.from_transport(transport)
    except Exception as e:
        sys.exit('error logging in: {}'.format(e))

    try:
        nloc = newest_local()
        nrem = newest_remote(sftp)
        print('latest download: {}'.format(nloc))
        print('latest data:     {}'.format(nrem))
        if nloc == None or nloc < nrem:
            print('downloading latest version')
            now = datetime.datetime.utcnow()
            ldir = now.strftime(prefix + '%Y-%m-%d_%H_%M_%S')
            getdir(sftp, directory, ldir)

            # create a symlink to the download
            linkname = prefix + 'latest'
            try:
                os.remove(linkname)
            except:
                pass
            os.symlink(ldir, linkname)
        else:
            print('nothing to download')
    except Exception as e:
        sys.exit('error getting data: {}'.format(e))
