# location of the shapefiles from VoGIS
SHP_DIR := ./vogis-wandern_latest/

# command to execute mkgmap (http://www.mkgmap.org.uk/)
MKGMAP := java -jar ~/bin/mkgmap.jar

# name of files containing all data
OUTPUT_NAME := vlbg_all

# uncomment to crop to the specified bounding box
#BBOX := -b 10.050378,47.148361,10.172417,47.221502

BBOX_VLBG := 46.8407,9.5308,47.5955,10.2372

# compressed output files
OUTPUT_COMP := \
	$(OUTPUT_NAME).kmz \
	$(OUTPUT_NAME)_decimated.kmz

# uncompressed output files
OUTPUT_UNCOMP := \
	$(OUTPUT_COMP:.kmz=.kml) \
	$(OUTPUT_COMP:.kmz=.osm)

# the generated map file
OUTPUT_MAP := garminmap-$(OUTPUT_NAME)/gmapsupp.img

# file converted from the shapefile
RAW_FILES := raw_wege.kml raw_tafeln.kml

.PHONY: all
all: $(OUTPUT_UNCOMP) $(OUTPUT_COMP) $(OUTPUT_MAP)

$(OUTPUT_UNCOMP): ./convert_vogis_trails.py $(RAW_FILES)
	./convert_vogis_trails.py $(BBOX) $(RAW_FILES) $(OUTPUT_NAME)

raw_%.kml: $(SHP_DIR)/wander_%.shp
	ogr2ogr -f "KML" -s_srs EPSG:31254 -t_srs EPSG:4326 $@ $<

%.kmz: %.kml
	@echo creating $@
	@ln $< doc.kml
	@zip $@ doc.kml >/dev/null
	@rm -f doc.kml

garminmap-%/gmapsupp.img: %.osm
	$(MKGMAP) \
		--output-dir=$(shell dirname $@) \
		--gmapsupp \
		--read-config=mkgmap/config \
		--input-file=$<

# A mapsforge (https://github.com/mapsforge/mapsforge/) file, can be used with
# android apps like "Locus Map". No custom style included, doesn't look very good.
.PHONY: mapsforge
mapsforge: mapsforge/vlbg_wanderwege.map

mapsforge/vlbg_wanderwege.map: vlbg_all.osm
	@mkdir -p mapsforge
	osmosis \
		--read-xml file=$< \
		--mapfile-writer file=$@ bbox=$(BBOX_VLBG)

.PHONY: clean
clean:
	rm -f $(RAW_FILES)
	rm -f $(OUTPUT_COMP)
	rm -f $(OUTPUT_UNCOMP)
	rm -rf garminmap*
